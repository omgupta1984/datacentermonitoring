﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DataCenterMonitoring.Core.Interface;
using DataCenterMonitoringAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DataCenterMonitoringAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MonitoringAPIController : Controller
    {
        private IMonitoringDataRepositery monitoringDataRepositery;

        public MonitoringAPIController(IMonitoringDataRepositery repo)
        {
            monitoringDataRepositery = repo;            
        }

        [HttpGet("GetMetricData")]
        public async Task<IActionResult>  GetMetricData()
        {
            try
            {
                var result= await monitoringDataRepositery.GetMetricDataAsync();
                return Ok(result);
            }
            catch(Exception e)
            {
                return StatusCode(500);
            }

            
        }

        [HttpGet("GetMachineLevelMetricData")]
        public async Task<IActionResult> GetMachineLevelData(int machineId)
        {
            try
            {
                var result = await monitoringDataRepositery.GetMachineLevelDataAsync(machineId);
                return Ok(result);
            }
            catch (Exception e)
            {
                return StatusCode(500);
            }
        }

        [HttpGet("GetClusterLevelMetricData")]
        public async Task<IActionResult> GetClusterLevelData(int cluterId)
        {
            try
            {
                var result = await monitoringDataRepositery.GetClusterLevelDataAsync(cluterId);
                return Ok(result);
            }
            catch (Exception e)
            {
                return StatusCode(500);
            }
        }
        [HttpGet("GetRackLevelMetricData")]
        public async Task<IActionResult> GetRackLevelData(int rackId)
        {
            try
            {
                var result = await monitoringDataRepositery.GetRackLevelDataAsync(rackId);
                return Ok(result);
            }
            catch (Exception e)
            {
                return StatusCode(500);
            }
        }

        
    }
}