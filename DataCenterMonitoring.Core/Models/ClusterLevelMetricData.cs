﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataCenterMonitoringAPI.Models
{
    public class ClusterLevelMetricData
    {
        public int ClusterId { get; set; }
        public string ClusterName { get; set; }
        public List<MachineLevelMetricData> MonitoringData { get; set; }
    }
}
