﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataCenterMonitoringAPI.Models
{
    public class RackLevelMetricData
    {
        public int RackId { get; set; }
        public string RackName { get; set; }
        public List<MachineLevelMetricData> MonitoringData { get; set; }
    }
}
