﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataCenterMonitoringAPI.Models
{
    public class MachineLevelMetricData
    {
        public int MachineId { get; set; }
        public int MachineIPAddress { get; set; }

        public int ApplicationId { get; set; }
        public string ApplicationName { get; set; }

        public int RamUtilization { get; set; }
        public int CPuUtilization { get; set; }
    }
}
