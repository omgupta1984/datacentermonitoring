﻿using DataCenterMonitoringAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataCenterMonitoring.Core.Interface
{
    public interface IMonitoringDataRepositery
    {
       Task<List<MachineLevelMetricData>> GetMetricDataAsync();
        Task<List<MachineLevelMetricData>> GetMachineLevelDataAsync(int machineId);
        Task<List<ClusterLevelMetricData>> GetClusterLevelDataAsync(int clusterId);
        Task<List<RackLevelMetricData>> GetRackLevelDataAsync(int rackId);
    }
}
