﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataCenterMonitoring.Core.Entity
{
    public class MonitoringData
    {
        public int DataCenterId { get; set; }
        public int RackId { get; set; }
        public string RackName { get; set; }
        public int ClusterId { get; set; }
        public string ClusterName { get; set; }
        public int ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public int MachineId { get; set; }
        public string MachineName { get; set; }
        public int MachineIpAddress { get; set; }
        public int CpuUtilization { get; set; }
        public int RamUtilization { get; set; }
        public DateTime CapturedDateTime { get; set; }
        public DateTime LastUpdatedTime { get; set; }
    }
}
