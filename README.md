# datacentermonitoring

de-normalize table structure
-----------------------------
CREATE TABLE [dbo].[tblDataCenterMachine](
	[DataCenterId] [bigint] NULL,
	[RackId] [bigint] NULL,
	[RackName] [varchar](50) NULL,
	[ClusterId] [bigint] NULL,
	[ClusterName] [varchar](50) NULL,
	[ApplicationId] [bigint] NULL,
	[ApplicationName] [varchar](50) NULL,
	[MachineId] [bigint] NULL,
	[MachineName] [varchar](50) NULL,
	[MachineIpAddress] [varchar](50) NULL,
	[CpuUtilization] [bigint] NULL,
	[RamUtilization] [bigint] NULL,
	[CapturedDateTime] [datetime] NULL,
	[LastUpdatedTime] [datetime] NULL
) ON [PRIMARY]

