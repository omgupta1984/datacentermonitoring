﻿using Dapper;
using DataCenterMonitoring.Core.Entity;
using DataCenterMonitoring.Core.Interface;
using DataCenterMonitoringAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCenterMonitoring.Infrastructure
{
   public class MonitoringDataRepositery : IMonitoringDataRepositery
    {
        private string connectionString = "Server=W109T3ZSQ2;Database=DataCenterMonitoring;User Id=sa;Password=Welcome8306;";
        public MonitoringDataRepositery()
        {

        }
        public async Task<List<ClusterLevelMetricData>> GetClusterLevelDataAsync(int clusterId)
        {
            var result = new List<ClusterLevelMetricData>();
            string sqlQuery = @"SELECT [DataCenterId]
                                ,[RackId]
                                ,[RackName]
                                ,[ClusterId]
                                ,[ClusterName]
                                ,[ApplicationId]
                                ,[ApplicationName]
                                ,[MachineId]
                                ,[MachineName]
                                ,[MachineIpAddress]
                                ,[CpuUtilization]
                                ,[RamUtilization]
                                ,[CapturedDateTime]
                                ,[LastUpdatedTime]
                            FROM [tblDataCenterMachine] where ClusterId = {0}";
            using (IDbConnection conn = new SqlConnection(connectionString))
            {
                var data = await conn.QueryAsync<MonitoringData>(string.Format(sqlQuery, clusterId));
                var listData = data.AsList();

                if (listData != null && listData.Count > 0)
                {
                    string RackName = listData.SingleOrDefault().ClusterName;
                    var metricData = new List<MachineLevelMetricData>();
                    metricData.AddRange(listData.Select(x => new MachineLevelMetricData
                    {
                        MachineId = x.MachineId,
                        MachineIPAddress = x.MachineIpAddress,
                        ApplicationId = x.ApplicationId,
                        ApplicationName = x.ApplicationName,
                        RamUtilization = x.RamUtilization,
                        CPuUtilization = x.CpuUtilization
                    }));
                    result.Add(new ClusterLevelMetricData { ClusterId = clusterId, ClusterName = RackName, MonitoringData = metricData });


                }
            }

                return result;
        }

        public async Task<List<MachineLevelMetricData>> GetMachineLevelDataAsync(int machineId)
        {
            var result = new List<MachineLevelMetricData>();
            string sqlQuery = @"SELECT [DataCenterId]
                                ,[RackId]
                                ,[RackName]
                                ,[ClusterId]
                                ,[ClusterName]
                                ,[ApplicationId]
                                ,[ApplicationName]
                                ,[MachineId]
                                ,[MachineName]
                                ,[MachineIpAddress]
                                ,[CpuUtilization]
                                ,[RamUtilization]
                                ,[CapturedDateTime]
                                ,[LastUpdatedTime]
                            FROM [tblDataCenterMachine] where MachineId = {0}";
            using (IDbConnection conn = new SqlConnection(connectionString))
            {
                var data = await conn.QueryAsync<MonitoringData>(string.Format(sqlQuery, machineId));
                var listData = data.AsList();

                if (listData != null && listData.Count > 0)
                {
                    result.AddRange(listData.Select(x => new MachineLevelMetricData
                    {
                        MachineId = x.MachineId,
                        MachineIPAddress = x.MachineIpAddress,
                        ApplicationId = x.ApplicationId,
                        ApplicationName = x.ApplicationName,
                        RamUtilization = x.RamUtilization,
                        CPuUtilization = x.CpuUtilization
                    }));
                }

            }
            return result;
        }

        public async Task<List<MachineLevelMetricData>> GetMetricDataAsync()
        {
            var result = new List<MachineLevelMetricData>();
            string sqlQuery = @"SELECT [DataCenterId]
                                ,[RackId]
                                ,[RackName]
                                ,[ClusterId]
                                ,[ClusterName]
                                ,[ApplicationId]
                                ,[ApplicationName]
                                ,[MachineId]
                                ,[MachineName]
                                ,[MachineIpAddress]
                                ,[CpuUtilization]
                                ,[RamUtilization]
                                ,[CapturedDateTime]
                                ,[LastUpdatedTime]
                            FROM [tblDataCenterMachine]";
            using (IDbConnection conn = new SqlConnection(connectionString))
            {
                var data = await conn.QueryAsync<MonitoringData>(sqlQuery);
                var listData = data.AsList();

                if (listData != null && listData.Count >0)
                {
                    result.AddRange(listData.Select(x => new MachineLevelMetricData { MachineId = x.MachineId, MachineIPAddress = x.MachineIpAddress, ApplicationId = x.ApplicationId,
                        ApplicationName = x.ApplicationName , RamUtilization = x.RamUtilization, CPuUtilization = x.CpuUtilization }));
                }                  

            }
            return result;
        }

        public async Task<List<RackLevelMetricData>> GetRackLevelDataAsync(int rackId)
        {
            var result = new List<RackLevelMetricData>();
            string sqlQuery = @"SELECT [DataCenterId]
                                ,[RackId]
                                ,[RackName]
                                ,[ClusterId]
                                ,[ClusterName]
                                ,[ApplicationId]
                                ,[ApplicationName]
                                ,[MachineId]
                                ,[MachineName]
                                ,[MachineIpAddress]
                                ,[CpuUtilization]
                                ,[RamUtilization]
                                ,[CapturedDateTime]
                                ,[LastUpdatedTime]
                            FROM [tblDataCenterMachine] where MachineId = {0}";
            using (IDbConnection conn = new SqlConnection(connectionString))
            {
                var data = await conn.QueryAsync<MonitoringData>(string.Format(sqlQuery, rackId));
                var listData = data.AsList();
                
                if (listData != null && listData.Count > 0)
                {
                    string RackName = listData.SingleOrDefault().RackName;
                    var metricData = new List<MachineLevelMetricData>();
                    metricData.AddRange(listData.Select(x => new MachineLevelMetricData
                    {
                        MachineId = x.MachineId,
                        MachineIPAddress = x.MachineIpAddress,
                        ApplicationId = x.ApplicationId,
                        ApplicationName = x.ApplicationName,
                        RamUtilization = x.RamUtilization,
                        CPuUtilization = x.CpuUtilization
                    }));
                    result.Add(new RackLevelMetricData { RackId = rackId, RackName = RackName, MonitoringData = metricData });
                }
                return result;
            }
        }
    }
}
